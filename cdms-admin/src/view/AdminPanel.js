import React, { useEffect, useState } from "react";
import { Route, Routes, useNavigate } from "react-router-dom";
import UserList from "../compontents/UserList";
import useAuth, { ax } from "../hooks/useAuth";
import { API_URL } from "../hooks/useSettings";
import CreateUser from "./CreateUser";
import UpdateUser from "./UpdateUser";

export default function AdminPanel() {
  const auth = useAuth();
  const [users, setUsers] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    if (users.length === 0 && auth.user) {
      ax.get(API_URL + "/users/")
        .then((res) => res.data)
        .then((data) => setUsers(data))
        .catch((e) => console.log("Ex", e));
    }
  }, []);

  const handleNewUser = (user) => {
    setUsers([...users, user]);
    navigate("");
  };

  const handleUpdateUser = (user) => {
    const index = users.findIndex(el => el.id == user.id)
    let new_users = users.copyWithin()
    new_users[index] = user
    setUsers([...new_users])
    navigate("");
  };

  return (
    <div className="w-full  py-4 flex justify-center content-center">
      <Routes>
        <Route path="/" element={<UserList data={users}></UserList>}></Route>
        <Route
          path="/create"
          element={<CreateUser callback={handleNewUser}></CreateUser>}
        ></Route>
        <Route
          path="/edit"
          element={<UpdateUser callback={handleUpdateUser}></UpdateUser>}
        ></Route>
      </Routes>
      {/* <UserList data={users}></UserList> */}
    </div>
  );
}
