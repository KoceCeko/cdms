import React, { useRef, useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import { ax } from "../hooks/useAuth";
import { API_URL } from "../hooks/useSettings";

export default function UpdateUser({ callback }) {
  const formRef = useRef();
  const {state} = useLocation();
  const user = state

  console.log("TEST", state)
  const [user2FALink, setUser2FALink] = useState(
    user.uses_otp ? "loading" : undefined
  );
  console.log("user", user)
  const handleButtonSubmit = (ev) => {
    ev.preventDefault()
    const data = {
      email: formRef.current.email.value,
      first_name: formRef.current.first_name.value,
      last_name: formRef.current.last_name.value,
    };
    console.log('TEST')
    ax.post(API_URL + `/users/${user.id}`, data)
      .then((res) => res.data)
      .then((user) => callback(user))
      .catch((er) => console.log("Error", er));
    console.log("data", data);
  };

  const handleCheckbox = (ev) => {
    console.log("ev", ev.target.checked);
    ax.post(API_URL + `/users/${user.id}/2fa/${ev.target.checked}`)
      .then((res) =>
        ev.target.checked ? setUser2FALink(res.data) : setUser2FALink(undefined)
      )
      .catch((err) => console.log("Error", err));
  };

  useEffect(() => {
    if (user.uses_otp) {
      ax.get(API_URL + `/users/${user.id}/2fa`).then((res) => setUser2FALink(res.data));
    }
  }, []);

  return (
    <div className="w-full flex justify-center content-center">
      <form className="w-1/2 py-10" ref={formRef}>
        <div className="relative z-0 mb-6 w-full group">
          <input
            type="email"
            name="email"
            className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
            placeholder=" "
            defaultValue={user.email}
            required
          />
          <label
            htmlFor="email"
            className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
          >
            Email address
          </label>
        </div>
        <div className="grid xl:grid-cols-2 xl:gap-6">
          <div className="relative z-0 mb-6 w-full group">
            <input
              type="text"
              name="first_name"
              id="first_name"
              defaultValue={user.first_name}
              className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
              placeholder=" "
              required
            />
            <label
              htmlFor="first_name"
              className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
            >
              First name
            </label>
          </div>
          <div className="relative z-0 mb-6 w-full group">
            <input
              type="text"
              name="last_name"
              id="last_name"
              defaultValue={user.last_name}
              className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
              placeholder=" "
              required
            />
            <label
              htmlFor="last_name"
              className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
            >
              Last name
            </label>
          </div>
        </div>

        <div className="flex">
          <input
            id="checkbox"
            aria-describedby="checkbox"
            type="checkbox"
            defaultChecked={user.uses_otp}
            onChange={handleCheckbox}
            className="w-4 h-4 mx-2 bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-blue-600 dark:ring-offset-gray-800"
          />

          <label className="text-sm text-gray-500 dark:text-gray-400 duration-300 ">
            Uses two factor authentication
          </label>
        </div>
        <div
          className={`w-full ${
            user2FALink === undefined ? "hidden" : ""
          } flex justify-between content-center py-4 h-1 `}
        >
          <label className="text-sm text-gray-500 dark:text-gray-400 duration-300 scale-75 top-3 ">
            2FA Link
          </label>

          <p class="text-sm dark:text-white">{user2FALink}</p>
        </div>
        <button
          type="submit"
          onClick={handleButtonSubmit}
          class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
        >
          Submit
        </button>
      </form>
    </div>
  );
}
