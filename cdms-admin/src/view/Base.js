import React from 'react'
import NavBar from '../compontents/NavBar'

export default function Base({children}) {
    return (
        <div className="object-fit flex-row">
          <header className="object-fit w-full flex-row content-space-between">
              <NavBar />
            </header>
              {children}
        </div>
    )
}
