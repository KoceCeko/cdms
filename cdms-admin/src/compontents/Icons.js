import React, { useRef, useState } from "react";
import { Link } from "react-router-dom";
import { ax } from "../hooks/useAuth";
import { API_URL } from "../hooks/useSettings";

export const CloudDownloadIcon = ({ path }) => {
  const params = new URLSearchParams();
  params.set("path", path);

  const handleDownload = (ev) => {
    ax.get(API_URL + "/dms/download-file?" + params).then((res) => {
      console.log("TEST", res);
      var blob = new Blob([res.data]);
      var url = URL.createObjectURL(blob);
      window.open(url);
    });
    // window.down(API_URL + "/users/download-file?" + params);
  };
  return (
    <button onClick={handleDownload}>
      <svg
        className="h-6 w-6"
        fill="none"
        viewBox="0 0 24 24"
        stroke="currentColor"
        stroke-width="2"
      >
        <path
          stroke-linecap="round"
          stroke-linejoin="round"
          d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M9 19l3 3m0 0l3-3m-3 3V10"
        />
      </svg>
      {/* <a href={} download>
        Click to download
      </a> */}
    </button>
  );
};

function readFile(file) {
  return new Promise((resolve, reject) => {
    // Create file reader
    let reader = new FileReader();
    // Register event listeners
    reader.addEventListener("loadend", (e) => resolve(e.target.result));
    reader.addEventListener("error", reject);

    // Read file
    reader.readAsArrayBuffer(file);
  });
}

async function getAsByteArray(file) {
  return new Uint8Array(await readFile(file));
}

export const CloudUploadIcon = ({
  path,
  is_dir = false,
  callback = () => {},
}) => {
  const fileInput = useRef();

  const handleUpload = (event, path) => {
    // setFile(event.target.files[0]);
    const file = event.target.files[0];
    let new_path = "";
    if (is_dir) {
      new_path = path + "/" + file.name;
    } else {
      new_path = path;
    }
    const params = new URLSearchParams();
    params.set("path", path);

    getAsByteArray(file).then((data) => {
      let formData = new FormData();
      formData.append("file", data);
      formData.append("path", new_path);
      console.log("TEST", data);
      ax({
        method: "POST",
        url: API_URL + `/dms/upload-file`,
        data: formData,
        headers: { "Content-Type": "multipart/form-data" },
      })
        .then((res) => res.data)
        .then((data) => callback(data))
        .catch((er) => console.log("upload err", er));
    });
  };

  return (
    <form>
      <label htmlFor={`input-file` + path}>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-6 w-6 cursor-pointer"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
          stroke-width="2"
          onClick={() => {
            // console.log('test', fileInput.current)
            fileInput.current.focus();
          }}
        >
          <path
            stroke-linecap="round"
            strokeLinejoin="round"
            d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12"
          ></path>
        </svg>
      </label>
      <input
        type="file"
        id={`input-file` + path}
        key={path}
        className="hidden"
        ref={fileInput}
        // onClick={()=> console.log('test', path)}
        onChange={(ev) => handleUpload(ev, path)}
      />
    </form>
  );
};

export function EditIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className="h-6 w-6 cursor-pointer"
      fill="none"
      viewBox="0 0 24 24"
      stroke="currentColor"
      stroke-width="2"
    >
      <path
        stroke-linecap="round"
        stroke-linejoin="round"
        d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"
      />
    </svg>
  );
}

export function TrashIcons() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      class="h-6 w-6"
      fill="none"
      viewBox="0 0 24 24"
      stroke="currentColor"
      stroke-width="2"
    >
      <path
        stroke-linecap="round"
        stroke-linejoin="round"
        d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
      />
    </svg>
  );
}

export function DirIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      class="h-6 w-6"
      fill="none"
      viewBox="0 0 24 24"
      stroke="currentColor"
      stroke-width="2"
    >
      <path
        stroke-linecap="round"
        stroke-linejoin="round"
        d="M9 13h6m-3-3v6m-9 1V7a2 2 0 012-2h6l2 2h6a2 2 0 012 2v8a2 2 0 01-2 2H5a2 2 0 01-2-2z"
      />
    </svg>
  );
}
