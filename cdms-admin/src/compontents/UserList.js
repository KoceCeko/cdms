import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { EditIcon } from "./Icons";

function ListItem({ user }) {
  const navigate = useNavigate()
  return (
    <li className="w-full flex justify-between px-4 py-2 border-b border-gray-200 dark:border-gray-600">
      <p>{user.email}</p>
      <button onClick={() => navigate('edit', {state: user})}>
        <EditIcon />
      </button>
    </li>
  );
}
export default function UserList({ data }) {
  const handleAddUser = () => {};

  return (
    <ul class="w-1/2 flex-row justify-center content-center text-sm font-medium text-gray-900 bg-white border border-gray-200 rounded-lg dark:bg-gray-700 dark:border-gray-600 dark:text-white">
      {data.map((user, index) => (
        <ListItem user={user} key={index}></ListItem>
      ))}
      <Link to="create">
        <li
          className="w-full px-4 cursor-pointer py-2 border-b border-gray-200 dark:border-gray-600"
          onClick={handleAddUser}
        >
          Add User +
        </li>
      </Link>
    </ul>
  );
}
