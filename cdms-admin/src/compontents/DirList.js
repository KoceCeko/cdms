import axios from "axios";
import React, { useRef, useState } from "react";
import { ax } from "../hooks/useAuth";
import { API_URL } from "../hooks/useSettings";
import { CloudDownloadIcon, CloudUploadIcon, DirIcon, TrashIcons } from "./Icons";

const ListItem = ({ item, path, setData = ()=>{} }) => {

  const handleDeleteButton = () => {
    const params = new URLSearchParams();
    params.set("path", path);
    console.log("do validate", path);
    ax.post(API_URL + `/dms/remove?${params}`)
      .then((res) => res.data)
      .then((data) => {
        setData(data)
        
      })
      .catch((ex) => console.log("ex", ex));
  };

  return (
    <li
      className={`py-2 px-4 ${
        item.is_dir ? "border border-gray-600" : " flex flex-row"
      } justify-between`}
    >
      <div className="flex justify-between w-full">
        <p className="text-base dark:text-white">{item.name}</p>
      <button
        className="text-base dark:text-white"
        onClick={handleDeleteButton}
      >
        <TrashIcons />
      </button>
      </div>

      <div className="flex">
      {item.is_dir ? (
        <DirList data={item.sub_el} path={path}></DirList>
      ) : (
        <>
          <CloudDownloadIcon path={path} />
          <CloudUploadIcon path={path} />
          </>
      )}
        </div>
    </li>
  );
};

export default function DirList({ data, setData = ()=>{}, path = "" }) {
  const [inputVisibility, setInputVisibility] = useState(false);
  const inputRef = useRef();
  const createDirectory = () => {
    setInputVisibility(!inputVisibility);
  };

  const handleKeyDown = (event) => {
    if (event.key === "Enter" && inputVisibility) {
      let new_path = path.replace("/", ":");
      let extension = inputRef.current.value;
      new_path = path + "/" + extension;

      const params = new URLSearchParams();
      params.set("path", new_path);
      console.log("do validate", new_path);
      ax.post(API_URL + `/dms/create?${params}`)
        .then((res) => res.data)
        .then((data) => {
          setInputVisibility(!inputVisibility)
          setData(data)
          console.log("update",setData)
        })
        .catch((ex) => console.log("ex", ex));
    }
  };

  return (
    <div className="w-full ">
      <ul className="flex-column w-full mx:auto w-80 text-sm font-medium text-gray-900 bg-white rounded-lg border border-gray-200 dark:bg-gray-700 dark:border-gray-600 dark:text-white">
        {data.map((el, index) => (
          <ListItem
            item={el}
            key={index}
            setData={setData}
            path={path + "/" + el.name}
          ></ListItem>
        ))}
        <div
          className={`py-2 px-4 flex w-full border border-gray-600 justify-between`}
        >
          <button onClick={() => createDirectory()}>
            <p className="text-base dark:text-white"><DirIcon/></p>
          </button>
          {inputVisibility ? (
            <input
              name="dirname"
              id="dirname"
              ref={inputRef}
              onKeyPress={handleKeyDown}
              className={`bg-gray-50 border ${
                inputVisibility ? "visible" : "invisible"
              }  border-gray-300 text-gray-900 text-sl rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-1/2  px-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white`}
              required
            />
          ) : (
            <div />
          )}
          <CloudUploadIcon path={path} is_dir={true} callback={setData} />
        </div>
      </ul>
    </div>
  );
}
