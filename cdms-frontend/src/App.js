import "./App.css";
import Login from "./compontents/Login";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate,
  useLocation,
} from "react-router-dom";
import TwoFA from "./compontents/TwoFA";
import useAuth, { AuthProvider, RequireAuth } from "./hooks/useAuth";
import Home from "./view/Home";
import Base from "./view/Base";
import Settings from "./view/Settings";
import AdminPanel from "./view/AdminPanel";

function App() {
  return (
    <div className="object-fit h-screen bg-[#282c34]">
      <Router>
        <AuthProvider>
          <Routes>
            <Route>
              <Route path="/login" exact element={<Login />} />
              <Route path="/2fa" exact element={<TwoFA />} />
              <Route
                path="/"
                element={
                  <RequireAuth>
                    <Base element={""}>
                      <Home />
                    </Base>
                  </RequireAuth>
                }
              />
              <Route
              path='/settings'
              exact
              element={
                  <RequireAuth>
                    <Base element={""}>
                      <Settings />
                    </Base>
                  </RequireAuth>
              }
              />
              {/* <Route
              path='/admin/*'
              element={
                  <RequireAuth>
                    <Base element={""}>
                      <AdminPanel />
                    </Base>
                  </RequireAuth>
              }
              /> */}
              <Route path="*" element={<Navigate to="/" />} />
            </Route>
          </Routes>
        </AuthProvider>
      </Router>
    </div>
  );
}

export default App;
