import { Navigate, useLocation, useNavigate } from "react-router-dom";
import { createContext, useContext, useState, useEffect } from "react";
import axios from "axios";
import { API_URL } from "./useSettings";

// const API_URL = useSettings().API_URL;

const AuthContext = createContext(null);
export const ax = axios.create()

function setBearer(token){
  if (!token) {
    return null
  }
  let interceptor = ax.interceptors.request.use(
    (config) => {
      config.headers.Authorization = `Bearer ${token}`;
      return config;
    },
    (error) => {
      console.log("Failed to set header");
      return Promise.reject(error);
    }
  );
  console.log('INTERCEPTOR', interceptor)
  return interceptor
};

export const removeBearer = (interceptor) => {
    console.log("REMOVE INTERCEPTOR", interceptor)
    ax.interceptors.request.eject(interceptor);
};

export function AuthProvider({ children }) {
  let [user, setUser] = useState(null);
  let [loading, setLoading] = useState(true);
  let [interceptor, setInterceptor] = useState(null);
  let [token, setToken] = useState(null)
  let [twoFA, setTwoFA] = useState(false)

  function getUser() {
    ax
      .get(API_URL + "/users/me")
      .then((res) => res.data)
      .then((data) => {
          setUser(data)
          if (!data.uses_otp){
            setTwoFA(true)
          }
          setLoading(false)
    })
      .catch((err) => {
        signout()
        console.log("Failed", err);
      });
  }
  

  let setUsesOtp = (value) => {
    setUser({...user, uses_otp: value})
  }

  let signin = (creds, callback = () => {}) => {
    const data = new FormData(creds);
    setLoading(true)
    console.log("SIGN IN", callback)
    ax
      .post(API_URL + "/login", data, {
        withCredentials: true,
        headers: {
          "Content-Type": `multipart/form-data; boundary=${data._boundary}`,
        },
      })
      .then((res) => res.data)
      .then((data) => {
        setToken(data.access_token)
      })
      .catch((e) => {
        signout(callback);
        console.log("Excepiton", e);
      });
  };

  let signout = (callback = () => {}) => {
    localStorage.removeItem('token')
    setUser(null)
    removeBearer(interceptor);
    setInterceptor(null);
    setToken(null);
  };

  let signTwoFA = (otp_pass, callback = () => {}) => {
    ax
      .post(API_URL + `/login/2fa?pass_value=${otp_pass}`)
      .then((res) => res.data)
      .then((data) => {
          setToken(data.access_token)
          setTwoFA(true)
      })
      .catch((err) => {
        signout(callback);
        console.log("two fa err", err);
        
      });
  };

  let signInGoogle = (data, callback = () => {}) => {
    ax
      .post(API_URL + "/login/google", data)
      .then((res) => res.data)
      .then((data) => {
          setToken(data.access_token)
      })
      .catch((err) => {
        signout(callback);
        console.log("two fa err", err);
        
      });
  };
  let value = { user, loading, twoFA, signin, signTwoFA, signout, signInGoogle, setUsesOtp};

  useEffect(() => {
      console.log("EFFECT token", token)
      if (token){
        localStorage.setItem("token", token);
            if(interceptor !== null) {
                removeBearer(interceptor)
            }
            const new_intercept = setBearer(token)
            setInterceptor(new_intercept)
        if (!user){
            getUser(setUser)
        }else{
            setLoading(false)
        }
      }
      else{
        const storage_token = localStorage.getItem('token')
        if (storage_token){
            setToken(storage_token)
        }else{
          setLoading(false)
        }
      }
  }, [token]);

  if (loading) {
    return <AuthContext.Provider value={value}>loading</AuthContext.Provider>;
  }

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}

export default function useAuth(callback) {
  return useContext(AuthContext);
}

export function RequireAuth({ children }) {
  let auth = useAuth();
  let location = useLocation();
  if (auth.loading){
      return <div />
  }
  if (!auth.user) {
    // Redirect them to the /login page, but save the current location they were
    // trying to go to when they were redirected. This allows us to send them
    // along to that page after they login, which is a nicer user experience
    // than dropping them off on the home page.
    return <Navigate to="/login" state={{ from: location }} replace />;
  }
  return children;
}
