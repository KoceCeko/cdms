import React, { useEffect, useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import useAuth, { ax } from "../hooks/useAuth";
import { API_URL } from "../hooks/useSettings";

export default function Settings() {
  const formRef = useRef();
  const [passVisible, setPassVisible] = useState(false);
  const auth = useAuth();
  const [user2FALink, setUser2FALink] = useState(
    auth.user.uses_otp ? "loading" : undefined
  );

  const navigate = useNavigate();

  useEffect(() => {
    if (auth.user.uses_otp) {
      ax.get(API_URL + "/users/2fa").then((res) => setUser2FALink(res.data));
    }
  }, []);
  console.log("Test", auth.user.uses_otp, auth.user.email);
  const handleButtonSubmit = (ev) => {
    ev.preventDefault();
    if (!passVisible) {
      return;
    }
    const passData = {
      password: formRef.current.password.value,
      repeat_password: formRef.current.repeat_password.value,
    };
    ax.post(API_URL + `/users/password`, passData)
      .then((res) => {
        alert("successfully changed password");
        navigate("/");
      })
      .catch((err) => console.log("Error", err));
    console.log("subbmit");
  };
  const onChangePassVisibility = () => {
    setPassVisible(!passVisible);
  };

  const handleCheckbox = (ev) => {
    console.log("ev", ev.target.checked);
    ax.post(API_URL + `/users/2fa/${ev.target.checked}`)
      .then((res) =>
        ev.target.checked ? setUser2FALink(res.data) : setUser2FALink(undefined)
      )
      .catch((err) => console.log("Error", err));
  };

  return (
    <div className="w-full flex justify-center content-center">
      <form className="w-1/2 py-10 flex flex-col" ref={formRef}>
        <button
          onClick={onChangePassVisibility}
          class="text-white my-6 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
        >
          Change Password
        </button>
        <div className={`${passVisible ? "" : "hidden"}`}>
          <div className="relative z-0 mb-6 w-full group ">
            <input
              type="password"
              name="password"
              id="password"
              defaultValue={""}
              className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
              placeholder=" "
              required
            />
            <label
              htmlFor="password"
              className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
            >
              Password
            </label>
          </div>
          <div className="relative z-0 mb-6 w-full group">
            <input
              type="password"
              name="repeat_password"
              id="repeat_password"
              defaultValue={""}
              className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
              placeholder=" "
              required
            />
            <label
              htmlFor="repeat_password"
              className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
            >
              Confirm password
            </label>
          </div>
        </div>

        <div className="flex">
          <input
            id="checkbox"
            aria-describedby="checkbox"
            type="checkbox"
            defaultChecked={auth.user.uses_otp}
            onChange={handleCheckbox}
            className="w-4 h-4 mx-2 bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-blue-600 dark:ring-offset-gray-800"
          />

          <label className="text-sm text-gray-500 dark:text-gray-400 duration-300 ">
            Uses two factor authentication
          </label>
        </div>
        <div
          className={`w-full ${
            user2FALink === undefined ? "hidden" : ""
          } flex justify-between content-center py-4 h-1 `}
        >
          <label className="text-sm text-gray-500 dark:text-gray-400 duration-300 scale-75 top-3 ">
            2FA Link
          </label>

          <p class="text-sm dark:text-white">{user2FALink}</p>
        </div>
        <button
          type="submit"
          onClick={handleButtonSubmit}
          class="text-white bg-blue-700 my-6 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
        >
          Submit
        </button>
      </form>
    </div>
  );
}
