import React, { useEffect, useState } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import DirList from "../compontents/DirList";
import useAuth, { ax } from "../hooks/useAuth";
import { API_URL } from "../hooks/useSettings";

export default function Home() {
  const auth = useAuth();
  const [data, setData] = useState(undefined);

  useEffect(() => {
    if (!data && auth.user) {
      ax.get(API_URL + "/dms/")
        .then((res) => res.data)
        .then((data) => setData(data))
        .catch((e) => console.log("Ex", e));
    }
  }, [data]);

  return (
    <body className="md:container md:mx-auto w-full flex justify-center content-top my-4">
      {data ? <DirList data={data} setData={(data) => setData(data)}></DirList> : <div />}
    </body>
  );
}
