import React, { useState, useRef, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import useAuth from "../hooks/useAuth";
import ReactGoogleLogin from "react-google-login";
import axios from "axios";
import { API_URL } from "../hooks/useSettings";

export default function Login() {
  const [credentials, setCredentials] = useState({
    username: "",
    password: "",
  });
  const navigate = useNavigate();
  const auth = useAuth();
  const ref = useRef();

  const handleSubmit = (ev) => {
    ev.preventDefault()
    auth.signin(ev.currentTarget, () => {
    });
  };

  const onGoogleSuccess = (data) => {
    let google_data = {
      username: data.profileObj.name,
      email: data.profileObj.email,
      first_name: data.profileObj.familyName,
      last_name: data.profileObj.givenName,
      id: data.profileObj.googleId,
      access_token: data.accessToken,
      id_token: data.tokenId,
    };
    console.log("TEST", data);
    auth.signInGoogle(google_data, () => {
    });
  };
  const onGoogleFail = (err) => {
    console.log("FAIL TEST", err);
  };

  // const test = () => {
  //   let data_in = {
  //     username: "username",
  //     email: "google@test.com",
  //     first_name: "gologolo",
  //     last_name: "worororo",
  //     id: "123456789123",
  //     id_token:
  //       "eyJhbGciOiJSUzI1NiIsImtpZCI6IjcyOTE4OTQ1MGQ0OTAyODU3MDQyNTI2NmYwM2U3MzdmNDVhZjI5MzIiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiNjU0MzE5NTk0Njc1LTg0dTd2dGNwbmZkcDY1NTAwZW1ndmFubnJjMTFkMGl1LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiNjU0MzE5NTk0Njc1LTg0dTd2dGNwbmZkcDY1NTAwZW1ndmFubnJjMTFkMGl1LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTEwNDg0NzczNjk0Njc3MTU0MzY3IiwiZW1haWwiOiJjZWtvLnZsYWRpbWlyQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdF9oYXNoIjoiOFdIMkljRUc4SWpxd3BYenBwaGNKUSIsIm5hbWUiOiJDZWtvIFZsYWRpbWlyIiwicGljdHVyZSI6Imh0dHBzOi8vbGgzLmdvb2dsZXVzZXJjb250ZW50LmNvbS9hLS9BT2gxNEdoTzkyaWVibE16bG03VUc5dGRYbDhKYWcwWmJfWmthclVrRWpJSD1zOTYtYyIsImdpdmVuX25hbWUiOiJDZWtvIiwiZmFtaWx5X25hbWUiOiJWbGFkaW1pciIsImxvY2FsZSI6ImVuIiwiaWF0IjoxNjQ3OTQ1MDgzLCJleHAiOjE2NDc5NDg2ODMsImp0aSI6IjQxZGRjOTk5MmQ2MjM0ZThlNjA0ZWRhYzgzNWQzODU2OGRlZDgxYzcifQ.cjGZN65MV5AeJhdmMAxYwWtqVoyz4h0hz5b31iDEcRvQlGXhOtMToS5ik4Fo0BiQ75ImsxJ5x81LXEZ7KNttR_yG8nhRBlsSg5xtht2TNxD9sCwWZ9Ew5ZDyzduFmX1kMDJnYggdZgA6NNnUtzxWPViDCuI0psHeFyHKt7VaqDCk1CSBlVzcGuoxVoFvYL7lSY1Ecbg6a-7z_K4nxPOheYVhBYzKTb6hxK5KFsVSI88MtKg1yHN2GYk6bwGcXRe7U5pcfcO2KBaYJl2jzHbhtOitf3EQ3zHHZ-6JzY3mrUIo8AbOT7Ba1NonQ205ZPQz6lgCiSc41zPvwxRPGQQovQ",
  //     access_token:
  //       "ya29.A0ARrdaM_jq2Ew9JDv37IgwkE00iF6GZFaYLxWZyN59YCN4DJfdg6_BMzzQlSZxxiSh-XXe5rVpbpy5nlcbXA0hYQjStPeqMM7Y1317vlBBumfTVP026pMDX8rB0Wt6LjFQ4RWcttrBBqLUb245TV5DVMIiDu03Q",
  //   };
  //   axios
  //     .post(API_URL + "/login/google", data_in)
  //     .then((res) => res.data)
  //     .then((data) => {
  //       console.log("backend fatch:", data);
  //     })
  //     .catch((e) => {
  //       console.log("Excepiton", e);
  //     });
  // };

  useEffect(() => {
    if (auth.user) {
      auth.user.uses_otp ? navigate("/2fa") : navigate("/");
    }
  }, [auth.user]);

  return (
    <div className="py-4">
      <div className="container mx-auto p-4 max-w-sm bg-white rounded-lg border border-gray-200 shadow-md sm:p-6 lg:p-8 dark:bg-gray-800 dark:border-gray-700">
        <form className="space-y-6" ref={ref} onSubmit={handleSubmit}>
          <h5 className="text-xl font-medium text-gray-900 dark:text-white">
            Sign in to our platform
          </h5>
          <div>
            <label
              htmlFor="email"
              className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
            >
              Your email
            </label>
            <input
              type="email"
              name="username"
              id="username"
              onChange={(ev) =>
                setCredentials({ ...credentials, email: ev.target.value })
              }
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
              required
            />
          </div>
          <div>
            <label
              htmlFor="password"
              className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
            >
              Your password
            </label>
            <input
              type="password"
              name="password"
              id="password"
              // onChange={(ev) =>
              //   setCredentials({ ...credentials, password: ev.target.value })
              // }
              placeholder="••••••••"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
              required
            />
          </div>
          <div className="flex items-start">
            <div className="flex items-start">
              <div className="flex items-center h-5">
                <input
                  id="remember"
                  aria-describedby="remember"
                  type="checkbox"
                  className="w-4 h-4 bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-blue-600 dark:ring-offset-gray-800"
                />
              </div>
              <div className="ml-3 text-sm">
                <label
                  htmlFor="remember"
                  className="font-medium text-gray-900 dark:text-gray-300"
                >
                  Remember me
                </label>
              </div>
            </div>
            <a
              href="#"
              className="ml-auto text-sm text-blue-700 hover:underline dark:text-blue-500"
            >
              Lost Password?
            </a>
          </div>
          <button
            type="submit"
            className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
          >
            Login to your account
          </button>
          <button
            type="button"
            className="text-white bg-[#24292F] hover:bg-[#24292F]/90 focus:ring-4 focus:ring-[#24292F]/50 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:focus:ring-gray-500 dark:hover:bg-[#050708]/30 mr-2 mb-2"
          >
            <svg
              className="mr-2 -ml-1 w-4 h-4"
              aria-hidden="true"
              focusable="false"
              data-prefix="fab"
              data-icon="github"
              role="img"
              // xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 496 512"
            >
              <path
                fill="currentColor"
                d="M165.9 397.4c0 2-2.3 3.6-5.2 3.6-3.3 .3-5.6-1.3-5.6-3.6 0-2 2.3-3.6 5.2-3.6 3-.3 5.6 1.3 5.6 3.6zm-31.1-4.5c-.7 2 1.3 4.3 4.3 4.9 2.6 1 5.6 0 6.2-2s-1.3-4.3-4.3-5.2c-2.6-.7-5.5 .3-6.2 2.3zm44.2-1.7c-2.9 .7-4.9 2.6-4.6 4.9 .3 2 2.9 3.3 5.9 2.6 2.9-.7 4.9-2.6 4.6-4.6-.3-1.9-3-3.2-5.9-2.9zM244.8 8C106.1 8 0 113.3 0 252c0 110.9 69.8 205.8 169.5 239.2 12.8 2.3 17.3-5.6 17.3-12.1 0-6.2-.3-40.4-.3-61.4 0 0-70 15-84.7-29.8 0 0-11.4-29.1-27.8-36.6 0 0-22.9-15.7 1.6-15.4 0 0 24.9 2 38.6 25.8 21.9 38.6 58.6 27.5 72.9 20.9 2.3-16 8.8-27.1 16-33.7-55.9-6.2-112.3-14.3-112.3-110.5 0-27.5 7.6-41.3 23.6-58.9-2.6-6.5-11.1-33.3 2.6-67.9 20.9-6.5 69 27 69 27 20-5.6 41.5-8.5 62.8-8.5s42.8 2.9 62.8 8.5c0 0 48.1-33.6 69-27 13.7 34.7 5.2 61.4 2.6 67.9 16 17.7 25.8 31.5 25.8 58.9 0 96.5-58.9 104.2-114.8 110.5 9.2 7.9 17 22.9 17 46.4 0 33.7-.3 75.4-.3 83.6 0 6.5 4.6 14.4 17.3 12.1C428.2 457.8 496 362.9 496 252 496 113.3 383.5 8 244.8 8zM97.2 352.9c-1.3 1-1 3.3 .7 5.2 1.6 1.6 3.9 2.3 5.2 1 1.3-1 1-3.3-.7-5.2-1.6-1.6-3.9-2.3-5.2-1zm-10.8-8.1c-.7 1.3 .3 2.9 2.3 3.9 1.6 1 3.6 .7 4.3-.7 .7-1.3-.3-2.9-2.3-3.9-2-.6-3.6-.3-4.3 .7zm32.4 35.6c-1.6 1.3-1 4.3 1.3 6.2 2.3 2.3 5.2 2.6 6.5 1 1.3-1.3 .7-4.3-1.3-6.2-2.2-2.3-5.2-2.6-6.5-1zm-11.4-14.7c-1.6 1-1.6 3.6 0 5.9 1.6 2.3 4.3 3.3 5.6 2.3 1.6-1.3 1.6-3.9 0-6.2-1.4-2.3-4-3.3-5.6-2z"
              ></path>
            </svg>
            Sign in with Github
          </button>
          <ReactGoogleLogin
            clientId="654319594675-84u7vtcpnfdp65500emgvannrc11d0iu.apps.googleusercontent.com"
            buttonText="Login"
            onSuccess={onGoogleSuccess}
            onFailure={onGoogleFail}
            cookiePolicy={"single_host_origin"}
            render={(props) => (
              <button
                type="button"
                onClick={props.onClick}
                disabled={props.disabled}
                className="text-white bg-[#4285F4] hover:bg-[#4285F4]/90 focus:ring-4 focus:ring-[#4285F4]/50 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:focus:ring-[#4285F4]/55 mr-2 mb-2"
              >
                <svg
                  className="mr-2 -ml-1 w-4 h-4"
                  aria-hidden="true"
                  focusable="false"
                  data-prefix="fab"
                  data-icon="google"
                  role="img"
                  // xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 488 512"
                >
                  <path
                    fill="currentColor"
                    d="M488 261.8C488 403.3 391.1 504 248 504 110.8 504 0 393.2 0 256S110.8 8 248 8c66.8 0 123 24.5 166.3 64.9l-67.5 64.9C258.5 52.6 94.3 116.6 94.3 256c0 86.5 69.1 156.6 153.7 156.6 98.2 0 135-70.4 140.8-106.9H248v-85.3h236.1c2.3 12.7 3.9 24.9 3.9 41.4z"
                  ></path>
                </svg>
                Sign in with Google
              </button>
            )}
          ></ReactGoogleLogin>
          <div className="text-sm font-medium text-gray-500 dark:text-gray-300">
            Not registered?{" "}
            <a
              href="#"
              className="text-blue-700 hover:underline dark:text-blue-500"
            >
              Create account
            </a>
          </div>
        </form>
      </div>
    </div>
  );
}
