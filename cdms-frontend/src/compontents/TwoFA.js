import React, {useEffect} from "react";
import { Navigate, useLocation, useNavigate } from "react-router-dom";
import useAuth from "../hooks/useAuth";

export default function TwoFA({ callback = () => null }) {
  const auth = useAuth();
  const navigate = useNavigate();
  const { state } = useLocation();

  const handeCodeInput = (ev) => {
    console.log("form", state);

    if (ev.target.value.length === 6) {
      auth.signTwoFA(ev.target.value, () => {
        navigate("/");
      });
    }
  };

  useEffect(() => {
    if (auth.user && auth.twoFA) {
      navigate("/");
    }
  }, [auth.user, auth.twoFA]);

  return (
    <div className="py-20">
      <div className="container mx-auto p-4 max-w-sm bg-white rounded-lg border border-gray-200 shadow-md sm:p-6 lg:p-8 dark:bg-gray-800 dark:border-gray-700">
        <form className="space-y-6">
          <h5 className="text-xl font-medium text-gray-900 dark:text-white">
            Enter authentication key
          </h5>
          <div>
            <label
              htmlFor="email"
              className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
            >
              code
            </label>
            <input
              type="code"
              name="code"
              id="code"
              onChange={handeCodeInput}
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
              required
            />
          </div>
        </form>
      </div>
    </div>
  );
}
