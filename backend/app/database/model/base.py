from sqlalchemy.ext.declarative import declarative_base, declared_attr
from typing import Any
Base = declarative_base()


# class Base(BaseDB):
#     id: Any
#     __name__: str
#     # Generate __tablename__ automatically
#     @declared_attr
#     def __tablename__(self) -> str:
#         return self.__name__.lower()+'s'
