from sqlalchemy import Column, Integer, String, Boolean
from app.database.model.base import Base


class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    email = Column(String, nullable=False)
    hashed_password = Column(String, nullable=False)
    first_name = Column(String, nullable=False)
    last_name = Column(String, nullable=False)
    role = Column(Integer, nullable=False)
    root_dir = Column(String, nullable=False)
    otp_secret = Column(String, nullable=True)
    uses_otp = Column(Boolean, nullable=True)
    is_active = Column(Boolean, nullable=False)
