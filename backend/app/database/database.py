from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from app.utils import get_settings
from typing import Generator

url = get_settings().postgres_database_url
engine = create_engine(url)
SessionLocal = sessionmaker(bind=engine, autocommit=False, autoflush=False)

def get_db() -> Generator:
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()
