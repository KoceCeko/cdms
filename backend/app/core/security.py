import json
import time
from hashlib import sha256

import requests
from fastapi.security import OAuth2PasswordBearer, SecurityScopes
from pydantic import ValidationError, AnyUrl
from typing import Union, Any, Optional, List
from datetime import datetime, timedelta
from fastapi import Depends, HTTPException, status, Security
from sqlalchemy.orm import Session
from app import crud
from app import schemas
from re import match
from jose import jwt
from app.utils import get_settings
from app.database.database import get_db
from app.database.model import User
from passlib.context import CryptContext
import pyotp
from google.oauth2 import id_token
from google.auth.transport import requests as g_requests


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

reusable_oauth2 = OAuth2PasswordBearer(
    tokenUrl='login',
    scopes={"client": "Client permissions.", "admin": "Administrator permissions.", "2fa": "Continue two fa"},
)

settings = get_settings()

ALGORITHM="HS256"

SECRET_KEY = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NDczMDIxMTYsInN1YiI6IjEifQ.qKD7TsbgpQS7p2r0fcyL6DeanuySCmef6pCAMrxXRM8'

GOOGLE_KEY = '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'
one_time_tokens = {}


def generate_otp_secret() -> str:
    secret_key = pyotp.random_base32()
    return secret_key


def create_2fa_link(secret_key: str, issuer) -> Any:
    link = pyotp.totp.TOTP(secret_key).provisioning_uri(name='backend.local.cdms.com', issuer_name="Vladimir Ceko")
    return link


def compare_two_fa_code(tfa_pass: str, secret_key):
    totp = pyotp.TOTP(secret_key)
    return totp.verify(tfa_pass)


def get_user_scopes(user: schemas.User) -> List[str]:
    if user.role == 0:
        return ['client', 'admin']
    else:
        return ['client']


def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    # raise HTTPException(status_code=400, detail='data?'+str(to_encode))
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def get_current_user(
        security_scopes: SecurityScopes, db: Session = Depends(get_db), token: schemas.Token = Depends(reusable_oauth2)
) -> User:
    if security_scopes.scopes:
        authenticate_value = f'Bearer scope="{security_scopes.scope_str}"'
    else:
        authenticate_value = f"Bearer"
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials userid"+str(security_scopes.scopes),
        headers={"WWW-Authenticate": authenticate_value},
    )
    try:
        # HS256 is symmetric algorithm
        payload = jwt.decode(
            token,
            # settings.SECRET_KEY,
            SECRET_KEY,
            algorithms=ALGORITHM
        )
        user_id = payload.get('sub')
    except:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Token not valid.",
            headers={"WWW-Authenticate": authenticate_value},
        )

    if user_id is None:
        raise credentials_exception
    token_scopes = payload.get("scopes", [])
    token_data = schemas.TokenData(scopes=token_scopes, user_id=user_id)
    # except (jwt.JWTError, ValidationError):
    user = crud.user.get(db, id=user_id)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    for scope in security_scopes.scopes:
        if scope not in token_data.scopes:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Not enough permissions "+scope+" "+str(token_data.scopes),
                headers={"WWW-Authenticate": authenticate_value},
            )
    return user


async def get_two_fa_user(
    current_user: User = Security(get_current_user, scopes=['2fa'])
):
    return current_user


async def get_admin_user(
    current_user: User = Security(get_current_user, scopes=["admin"])
):
    return current_user


async def get_client_user(
    current_user: User = Security(get_current_user, scopes=["client"])
):
    return current_user


def verify_password(plain_password: str, hashed_password: str) -> bool:
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password: str) -> str:
    return pwd_context.hash(password)


def verify_password_reset_token(token: str) -> Optional[str]:
    try:
        decoded_token = jwt.decode(token, settings.SECRET_KEY, algorithms=[ALGORITHM])
        return decoded_token["email"]
    except jwt.JWTError:
        return None


def hash_string(value: str):
    hash_val = sha256(bytes(value, encoding='utf8'))
    return hash_val.hexdigest()


XSSRegex = "<[^\\w<>]*(?:[^<>\"'\\s]*:)?[^\\w<>]*(?:\\W*s\\W*c\\W*r\\W*i\\W*p\\W*t|\\W*f\\W*o\\W*r\\W*m|\\W*s\\W*t\\W" \
           "*y\\W*l\\W*e|\\W*s\\W*v\\W*g|\\W*m\\W*a\\W*r\\W*q\\W*u\\W*e\\W*e|(" \
           "?:\\W*l\\W*i\\W*n\\W*k|\\W*o\\W*b\\W*j\\W*e\\W*c\\W*t|\\W*e\\W*m\\W*b\\W*e\\W*d|\\W*a\\W*p\\W*p\\W*l\\W*e" \
           "\\W*t|\\W*p\\W*a\\W*r\\W*a\\W*m|\\W*i?\\W*f\\W*r\\W*a\\W*m\\W*e|\\W*b\\W*a\\W*s\\W*e|\\W*b\\W*o\\W*d\\W*y" \
           "|\\W*m\\W*e\\W*t\\W*a|\\W*i\\W*m\\W*a?\\W*g\\W*e?|\\W*v\\W*i\\W*d\\W*e\\W*o|\\W*a\\W*u\\W*d\\W*i\\W*o|\\W" \
           "*b\\W*i\\W*n\\W*d\\W*i\\W*n\\W*g\\W*s|\\W*s\\W*e\\W*t|\\W*i\\W*s\\W*i\\W*n\\W*d\\W*e\\W*x|\\W*a\\W*n\\W*i" \
           "\\W*m\\W*a\\W*t\\W*e)[^>\\w])|(?:<\\w[\\s\\S]*[\\s\\0\\/]|['\"])(" \
           "?:formaction|style|background|src|lowsrc|ping|on(?:d(?:e(?:vice(?:(" \
           "?:orienta|mo)tion|proximity|found|light)|livery(?:success|error)|activate)|r(?:ag(?:e(?:n(?:ter|d)|xit)|(" \
           "?:gestur|leav)e|start|drop|over)?|op)|i(?:s(?:c(?:hargingtimechange|onnect(?:ing|ed))|abled)|aling)|ata(" \
           "?:setc(?:omplete|hanged)|(?:availabl|chang)e|error)|urationchange|ownloading|blclick)|Moz(?:M(" \
           "?:agnifyGesture(?:Update|Start)?|ouse(?:PixelScroll|Hittest))|S(?:wipeGesture(" \
           "?:Update|Start|End)?|crolledAreaChanged)|(?:(?:Press)?TapGestur|BeforeResiz)e|EdgeUI(?:C(" \
           "?:omplet|ancel)|Start)ed|RotateGesture(?:Update|Start)?|A(?:udioAvailable|fterPaint))|c(?:o(?:m(?:p(" \
           "?:osition(?:update|start|end)|lete)|mand(?:update)?)|n(?:t(?:rolselect|extmenu)|nect(?:ing|ed))|py)|a(?:(" \
           "?:llschang|ch)ed|nplay(?:through)?|rdstatechange)|h(?:(?:arging(?:time)?ch)?ange|ecking)|(" \
           "?:fstate|ell)change|u(?:echange|t)|l(?:ick|ose))|m(?:o(?:z(?:pointerlock(?:change|error)|(" \
           "?:orientation|time)change|fullscreen(?:change|error)|network(?:down|up)load)|use(?:(?:lea|mo)ve|o(" \
           "?:ver|ut)|enter|wheel|down|up)|ve(?:start|end)?)|essage|ark)|s(?:t(?:a(?:t(" \
           "?:uschanged|echange)|lled|rt)|k(?:sessione|comma)nd|op)|e(?:ek(?:complete|ing|ed)|(?:lec(?:tstar)?)?t|n(" \
           "?:ding|t))|u(?:ccess|spend|bmit)|peech(?:start|end)|ound(?:start|end)|croll|how)|b(?:e(?:for(?:e(?:(" \
           "?:scriptexecu|activa)te|u(?:nload|pdate)|p(?:aste|rint)|c(?:opy|ut)|editfocus)|deactivate)|gin(" \
           "?:Event)?)|oun(?:dary|ce)|l(?:ocked|ur)|roadcast|usy)|a(?:n(?:imation(" \
           "?:iteration|start|end)|tennastatechange)|fter(?:(?:scriptexecu|upda)te|print)|udio(" \
           "?:process|start|end)|d(?:apteradded|dtrack)|ctivate|lerting|bort)|DOM(?:Node(?:Inserted(" \
           "?:IntoDocument)?|Removed(?:FromDocument)?)|(?:CharacterData|Subtree)Modified|A(" \
           "?:ttrModified|ctivate)|Focus(?:Out|In)|MouseScroll)|r(?:e(?:s(?:u(?:m(" \
           "?:ing|e)|lt)|ize|et)|adystatechange|pea(?:tEven)?t|movetrack|trieving|ceived)|ow(?:s(" \
           "?:inserted|delete)|e(?:nter|xit))|atechange)|p(?:op(?:up(?:hid(?:den|ing)|show(?:ing|n))|state)|a(?:ge(" \
           "?:hide|show)|(?:st|us)e|int)|ro(?:pertychange|gress)|lay(?:ing)?)|t(?:ouch(?:(?:lea|mo)ve|en(" \
           "?:ter|d)|cancel|start)|ime(?:update|out)|ransitionend|ext)|u(?:s(?:erproximity|sdreceived)|p(" \
           "?:gradeneeded|dateready)|n(?:derflow|load))|f(?:o(?:rm(?:change|input)|cus(?:out|in)?)|i(" \
           "?:lterchange|nish)|ailed)|l(?:o(?:ad(?:e(?:d(?:meta)?data|nd)|start)?|secapture)|evelchange|y)|g(" \
           "?:amepad(?:(?:dis)?connected|button(?:down|up)|axismove)|et)|e(?:n(?:d(?:Event|ed)?|abled|ter)|rror(" \
           "?:update)?|mptied|xit)|i(?:cc(?:cardlockerror|infochange)|n(?:coming|valid|put))|o(?:(?:(" \
           "?:ff|n)lin|bsolet)e|verflow(?:changed)?|pen)|SVG(?:(?:Unl|L)oad|Resize|Scroll|Abort|Error|Zoom)|h(?:e(" \
           "?:adphoneschange|l[dp])|ashchange|olding)|v(?:o(?:lum|ic)e|ersion)change|w(?:a(?:it|rn)ing|heel)|key(" \
           "?:press|down|up)|(?:AppComman|Loa)d|no(?:update|match)|Request|zoom))[\\s\\0]*= "
XSSRegex1 = "/((\\%3C)|<)((\\%2F)|\\/)*[a-z0-9\\%]+((\\%3E)|>)/ix"
XSSRegex2 = "/((\\%3C)|<)[^\\n]+((\\%3E)|>)/I"

SQLInjectionRegex = "('(''|[^'])*')|(;)|(\b(ALTER|CREATE|DELETE|DROP|EXEC(UTE){0,1}|INSERT( +INTO){0," \
                    "1}|MERGE|SELECT|UPDATE|UNION( +ALL){0,1})\b) "
SQLInjectionRegex1 = "/(\\%27)|(\\')|(\\-\\-)|(\\%23)|(#)/ix"
SQLInjectionRegex2 = "/((\\%3D)|(=))[^\\n]*((\\%27)|(\\')|(\\-\\-)|(\\%3B)|(;))/i"
SQLInjectionRegex3 = "/\\w*((\\%27)|(\\'))((\\%6F)|o|(\\%4F))((\\%72)|r|(\\%52))/ix"
SQLInjectionRegex4 = "/((\\%27)|(\\'))union/ixx"


def check_xss(value: str):
    match0 = match(SQLInjectionRegex, value)
    match1 = match(SQLInjectionRegex1, value)
    match2 = match(SQLInjectionRegex2, value)
    return match0 or match1 or match2


def check_sql_injection(value: str):
    match0 = match(SQLInjectionRegex, value)
    match1 = match(SQLInjectionRegex1, value)
    match2 = match(SQLInjectionRegex2, value)
    match3 = match(SQLInjectionRegex3, value)
    match4 = match(SQLInjectionRegex4, value)
    return match0 or match1 or match2 or match3 or match4


def check_google_token(accessToken: str = ''):
    try:
        # Specify the CLIENT_ID of the app that accesses the backend:
        idinfo = id_token.verify_oauth2_token(accessToken, g_requests.Request())
        return idinfo['email_verified']
    except ValueError as ev:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Could not validate credentials jwt "+str(ev),
        )

    raise HTTPException(
        status_code=status.HTTP_403_FORBIDDEN,
        detail="Could not validate credentials jwt ",
    )
