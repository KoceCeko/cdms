import os.path
from pathlib import Path
from fastapi import HTTPException
from typing import Any, Dict, Optional, Union

from sqlalchemy.orm import Session

# from app.core.security import get_password_hash, verify_password
import app.core.security
from app.crud.base import CRUDBase
from app.database.model.user import User
from app.schemas.user import UserCreate, UserUpdate, UserGoogleCreate
from app.core.security import hash_string, generate_otp_secret, compare_two_fa_code
import pyotp


class CRUDUser(CRUDBase[User, UserCreate, UserUpdate]):
    def create(self, db: Session, *, obj_in: UserCreate) -> User:
        db_obj = User(
            email=obj_in.email,
            hashed_password=hash_string(obj_in.password),
            first_name=obj_in.first_name,
            last_name=obj_in.last_name,
            is_active=True,
            uses_otp=False,
            role=1,
            root_dir=obj_in.first_name+'_'+obj_in.last_name,
            otp_secret=generate_otp_secret(),
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj


    def create_external(self, db: Session, *, obj_in: UserGoogleCreate) -> User:
        db_obj = User(
            email=obj_in.email,
            hashed_password=hash_string(obj_in.external_id),
            first_name=obj_in.first_name,
            last_name=obj_in.last_name,
            is_active=True,
            uses_otp=False,
            role=1,
            root_dir=obj_in.root_dir,
            otp_secret=generate_otp_secret(),
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def update(
        self, db: Session, *, db_obj: User, obj_in: Union[UserUpdate, Dict[str, Any]]
    ) -> User:
        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.dict(exclude_unset=True)
        if 'password' in update_data.keys():
            hashed_password = hash_string(update_data["password"])
            del update_data["password"]
            update_data["hashed_password"] = hashed_password
        return super().update(db, db_obj=db_obj, obj_in=update_data)

    def activate_2fa(
        self, db: Session, *, obj_in: User
    ) -> User:
        if isinstance(obj_in, dict):
            update_data = obj_in
        db_obj = app.core.security.get_current_user()
        return super().update(db, db_obj=db_obj, obj_in=update_data)

    @staticmethod
    def get_user_by_name(db: Session, full_name):
        return db.query(User).filter(User.full_name == full_name).first()

    @staticmethod
    def get_user_by_email(db: Session, email):
        return db.query(User).filter(User.email == email).first()

    @staticmethod
    def get_user_by_id(db: Session, id):
        return db.query(User).filter(User.id == id).first()

    def get_otp_secret(self, db: Session, email) -> str:
        return self.get_user_by_email(db, email).otp_secret

    def authenticate(self, db: Session, *, email: str, password: str) -> bool:
        user = self.get_user_by_email(db, email)
        if not user:
            return None
        request_hash = hash_string(password)
        # if user.uses_otp:
        #     if not compare_two_fa_code(otp_pass, user.otp_secret):
        #         return None
        if not request_hash == user.hashed_password:
            return None
        return user

    def is_active(self, user: User) -> bool:
        return user.is_active

    def get_subelements(self, path: Path):
        elements = []
        glob = path.glob('*')
        for p in glob:
            path_dict = {}
            path_dict['name'] = p.relative_to(path)
            path_dict['is_dir'] = p.is_dir()
            if p.is_dir():
                path_dict['sub_el'] = self.get_subelements(p)
            elements.append(path_dict)
        return elements

    def get_root_dir(self, user: User):
        if user.role == 1:
            base_dir = os.path.join(os.getcwd(), 'files')
        elif user.root_dir == '':
            base_dir = os.path.join(os.getcwd(), 'files', 'temp')
        else:
            base_dir = os.path.join(os.getcwd(), 'files', user.root_dir)
        return base_dir

    def get_directory(self, user: User):
        base_dir = self.get_root_dir(user)

        if not os.path.exists(base_dir):
            os.makedirs(base_dir)

        base_path = Path(base_dir)
        glob = base_path.glob('*')
        if not base_path.exists():
            base_path.mkdir()
        ret_val = []
        for p in glob:
            path_dict = {}
            path_dict['name'] = p.relative_to(base_dir)
            path_dict['is_dir'] = p.is_dir()
            if p.is_dir():
                path_dict['sub_el'] = self.get_subelements(p)
            ret_val.append(path_dict)
        return ret_val

    def create_dir(self, user: User, dir):
        base_dir = self.get_root_dir(user)
        if dir[0] == '/':
            dir = dir[1:]
        new_path = os.path.join(base_dir, dir)
        os.makedirs(new_path)
        return self.get_directory(user)

    def remove_dir(self, user: User, dir):
        base_dir = self.get_root_dir(user)
        if dir[0] == '/':
            dir = dir[1:]
        path = os.path.join(base_dir, dir)
        if os.path.isdir(path):
            try:
                os.removedirs(path)
            except:
                raise HTTPException(status_code=400, detail='Directory contains files')
        else:
            os.remove(path)
        return self.get_directory(user)

    def get_file_path(self, user: User, path: str = ''):
        base_dir = self.get_root_dir(user)
        if path[0] == '/':
            path = path[1:]
        full_path = os.path.join(base_dir, path)
        return full_path

user = CRUDUser(User)
