from typing import Optional

from pydantic import BaseModel


# Shared properties
class UserBase(BaseModel):
    id: Optional[int] = None
    email: Optional[str] = None
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    is_active: Optional[bool] = True
    root_dir: Optional[str] = None
    uses_otp: Optional[bool] = False

    class Config:
        orm_mode = True


class UserCreate(UserBase):
    password: str


class UserUpdate(UserBase):
    password: Optional[str] = None


class UserPassword(BaseModel):
    password: Optional[str] = ''
    repeat_password: Optional[str] = '!'


class User(UserBase):
    role: Optional[int] = 0
    pass


class UserGoogle(UserBase):
    access_token: Optional[str] = ''
    username: Optional[str] = ''
    id: Optional[str] = ''
    id_token: Optional[str] = ''
    role: Optional[int] = 0


class UserGoogleCreate(UserGoogle):

    external_id: Optional[str] = ''

    def __init__(self, user_in: UserGoogle):
        super(UserGoogleCreate, self).__init__()
        self.email = user_in.email
        self.first_name = user_in.first_name
        self.last_name = user_in.last_name
        self.root_dir = user_in.username
        self.username = user_in.username
        self.external_id = user_in.id


class Client(User):
    role: Optional[int] = 1
    pass


class Administrator(User):
    role: Optional[int] = 2
    pass
