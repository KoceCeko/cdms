from typing import Optional

from pydantic import BaseModel


# Shared properties
class File(BaseModel):
    file: Optional[bytes] = None
    path: Optional[str] = ''