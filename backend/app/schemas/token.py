from typing import Optional, List
from pydantic import BaseModel


class Token(BaseModel):
    access_token: str
    token_type: str
    scopes: List[str] = []


class TwoFA(BaseModel):
    two_factor_auth_code: str


class TokenData(BaseModel):
    scopes: List[str] = []


class TokenPayload(BaseModel):
    sub: Optional[int] = None