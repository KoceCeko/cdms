import os.path

import pydantic
from fastapi import APIRouter, Depends, HTTPException, Form
from fastapi.responses import FileResponse
from typing import List, Any
from sqlalchemy.orm import Session

import app.schemas
from app import crud
from app.database.database import get_db
from app import schemas
from app.core.security import get_current_user, get_client_user

router = APIRouter()


@router.get("/")
def read_user_dirs(
    db: Session = Depends(get_db),
    user: schemas.User = Depends(get_client_user),
) -> Any:
    """
    Retrieve users.
    """
    items = crud.user.get_directory(user)
    return items


@router.post("/create", response_model=Any)
async def create_dir(current_user: schemas.User = Depends(get_client_user), path: str = ''):
    if '..' in path:
        raise HTTPException(status_code=400, detail='Wrong input name for dir')
    return crud.user.create_dir(user=current_user, dir=path)


@router.post("/remove", response_model=Any)
async def create_dir(current_user: schemas.User = Depends(get_client_user), path: str = ''):
    if '..' in path:
        raise HTTPException(status_code=400, detail='Wrong input name for dir')
    return crud.user.remove_dir(user=current_user, dir=path)


@router.post('/upload-file')
async def upload_dms_file(file: str = Form(...), path: str = Form(...), user: schemas.User = Depends(get_current_user)):
    """
    Upload file to the storage
    """
    if '..' in path:
        raise HTTPException(status_code=400, detail='Wrong input name for dir')
    exact_path = crud.user.get_file_path(user, path)
    # return exact_path
    new_file = bytearray()
    bytes = file.split(',')
    for b in bytes:
        new_file.append(int(b))
    with open(exact_path, 'wb') as f:
        f.write(new_file)
    return crud.user.get_directory(user=user)


@router.get('/download-file')
def download_file(path: str = 0, current_user: schemas.User = Depends(get_client_user)):
    # return path
    if '..' in path:
        raise HTTPException(status_code=400, detail='Wrong input name for dir')
    exact_path = crud.user.get_file_path(current_user, path)

    def iterfile(f_path):
        with open(f_path, mode="rb") as file_like:  #
            test = file_like.read()
            # yield from file_like  #
            return test
    return FileResponse(exact_path)
