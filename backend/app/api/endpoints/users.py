import os.path

import pydantic
from fastapi import APIRouter, Depends, HTTPException, File, UploadFile, Form
from fastapi.responses import FileResponse, StreamingResponse
from typing import List, Any
from sqlalchemy.orm import Session
from fastapi.security import OAuth2PasswordBearer

import app.schemas
from app import crud
from app.database.database import get_db
from app import schemas
from app.core.security import hash_string, get_current_user, create_2fa_link, get_client_user, get_admin_user, compare_two_fa_code

router = APIRouter()


@router.get("/", response_model=List[schemas.User])
def read_users(
    db: Session = Depends(get_db),
    skip: int = 0,
    limit: int = 100,
    user: schemas.User = Depends(get_admin_user),
) -> Any:
    """
    Retrieve users.
    """
    users = crud.user.get_multi(db, skip=skip, limit=limit)
    return users


@router.post("/", response_model=schemas.User)
def create_user(
    *,
    db: Session = Depends(get_db),
    user_in: schemas.UserCreate,
    user: schemas.User = Depends(get_admin_user)
) -> Any:
    """
    Create new user.
    """
    check_user = crud.user.get_user_by_email(db, email=user_in.email)

    if check_user:
        raise HTTPException(
            status_code=400,
            detail="The user with this username already exists in the system.",
        )
    new_user = crud.user.create(db, obj_in=user_in)
    return new_user


@router.post("/password", response_model=schemas.User)
async def update_password(
    *,
    db: Session = Depends(get_db),
    passwords: schemas.UserPassword,
    user: schemas.User = Depends(get_client_user)
) -> Any:
    """
    Update password.
    """
    update_user = crud.user.get_user_by_id(db, id=user.id)
    if len(passwords.password) < 4:
        raise HTTPException(
            status_code=400,
            detail="Password is to short.",
        )
    elif passwords.password != passwords.repeat_password:
        raise HTTPException(
            status_code=400,
            detail="Passwords didn't match.",
        )
    if not update_user:
        raise HTTPException(
            status_code=400,
            detail="The user with this username already exists in the system.",
        )

    new_user = crud.user.update(db, obj_in=passwords, db_obj=update_user)
    return new_user

@router.get("/me", response_model=schemas.User)
async def read_users_me(current_user: schemas.User = Depends(get_current_user)):
    return current_user


@router.get("/2fa", response_model=pydantic.AnyUrl)
def get_2fa_link(db: Session = Depends(get_db), user: schemas.User = Depends(get_client_user)):
    """
    Create 2fa link
    """
    if not user.uses_otp:
        raise HTTPException(
            status_code=400,
            detail="The user doesn't use two factor authentication.",
        )
    secret_key = crud.user.get_otp_secret(db, user.email)
    return create_2fa_link(secret_key, user.first_name+' '+user.last_name)


@router.post("/2fa/{activate}", response_model=pydantic.AnyUrl)
def get_2fa_link(db: Session = Depends(get_db), user: schemas.User = Depends(get_client_user), activate: bool = False):
    """
    Activate 2FA
    """
    form_data = {'uses_otp': activate}
    update = crud.user.update(db, db_obj=user, obj_in=form_data)
    secret_key = crud.user.get_otp_secret(db, update.email)
    return create_2fa_link(secret_key, update.first_name+' '+update.last_name)


@router.post("/{id}", response_model=schemas.User)
def update_user(
    *,
    db: Session = Depends(get_db),
    user_in: schemas.UserUpdate,
    id: int,
    user: schemas.User = Depends(get_admin_user)
) -> Any:
    """
    Create new user.
    """
    update_user = crud.user.get_user_by_id(db, id=id)

    if not update_user:
        raise HTTPException(
            status_code=400,
            detail="The user with this username already exists in the system.",
        )
    new_user = crud.user.update(db, obj_in=user_in, db_obj=update_user)
    return new_user

@router.get("/{id}/2fa", response_model=pydantic.AnyUrl)
def set_2fa_by_id(id: int = -1, db: Session = Depends(get_db), user: schemas.User = Depends(get_admin_user)):
    """
    Create 2fa link
    """

    change_user = crud.user.get_user_by_id(db, id)
    if not change_user.uses_otp:
        raise HTTPException(
            status_code=400,
            detail="The user doesn't use two factor authentication.",
        )
    secret_key = crud.user.get_otp_secret(db, change_user.email)
    return create_2fa_link(secret_key, change_user.first_name+' '+change_user.last_name)


@router.post("/{id}/2fa/{activate}", response_model=pydantic.AnyUrl)
def set_2fa_by_id(id: int = -1, db: Session = Depends(get_db), user: schemas.User = Depends(get_admin_user), activate: bool = False):
    """
    Activate 2FA
    """
    form_data = {'uses_otp': activate}
    change_user = crud.user.get_user_by_id(db, id)
    update = crud.user.update(db, db_obj=change_user, obj_in=form_data)
    secret_key = crud.user.get_otp_secret(db, update.email)
    return create_2fa_link(secret_key, update.first_name+' '+update.last_name)
