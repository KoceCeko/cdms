from datetime import timedelta
from typing import Any, ByteString

from fastapi import APIRouter, Body, Depends, HTTPException, Response, Form
from fastapi.responses import RedirectResponse
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from app import crud, schemas
from app.database.model import User
from app.database.database import get_db
from app.core import security
from app.utils import settings
from app.core.security import (verify_password_reset_token, get_password_hash, get_two_fa_user,
                               get_current_user, compare_two_fa_code, get_user_scopes)

router = APIRouter()

@router.post("", response_model=schemas.Token)
def login_access_token(
    response: Response, db: Session = Depends(get_db), form_data: OAuth2PasswordRequestForm = Depends(),
) -> Any:
    """
    OAuth2 compatible token login, get an access token for future requests
    """
    user = crud.user.authenticate(
        db, email=form_data.username, password=form_data.password
    )
    if not user:
        raise HTTPException(status_code=400, detail="Incorrect email or password")
    elif not crud.user.is_active(user):
        raise HTTPException(status_code=400, detail="Inactive user")
    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    scopes = []
    if user.uses_otp:
        scopes.append('2fa')
    elif len(form_data.scopes) > 0:
        scopes = form_data.scopes
    else:
        scopes = get_user_scopes(user)
    access_token = security.create_access_token(
        {"sub": str(user.id), "scopes": scopes}, expires_delta=access_token_expires
    )
    return {
        "access_token": access_token,
        "token_type": "bearer",
        "scopes": scopes,
    }


@router.post("/2fa", response_model=schemas.Token)
def two_factor_authentication(
        db: Session = Depends(get_db),
        user: schemas.User = Depends(get_two_fa_user),
        pass_value: str = ' def',
) -> Any:
    if not user.uses_otp:
        raise HTTPException(
            status_code=400,
            detail="The user doesn't use two factor authentication.",
        )
    if not user:
        raise HTTPException(status_code=400, detail="Incorrect email or password")
    ret = compare_two_fa_code(pass_value, user.otp_secret)
    ret = True
    if not ret:
        raise HTTPException(status_code=400, detail="Failed 2 factor authentication"+pass_value)
    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    scopes = get_user_scopes(user)
    access_token = security.create_access_token(
        {"sub": str(user.id), "scopes": scopes}, expires_delta=access_token_expires
    )
    # raise HTTPException(status_code=400, detail="Failed 2 factor authentication"+str(scopes))
    return {
        "access_token": access_token,
        "token_type": "bearer",
    }


@router.post("/test-token", response_model=schemas.User)
def test_token(db: Session = Depends(get_db), user: schemas.User = Depends(get_current_user)) -> schemas.User:
    """
    Test access token
    """
    return user

# @router.post("/password-recovery/{email}", response_model=schemas.Msg)
# def recover_password(email: str, db: Session = Depends(get_db)) -> Any:
#     """
#     Password Recovery
#     """
#     user = crud.user.get_by_email(db, email=email)
#
#     if not user:
#         raise HTTPException(
#             status_code=404,
#             detail="The user with this username does not exist in the system.",
#         )
#     password_reset_token = generate_password_reset_token(email=email)
#     send_reset_password_email(
#         email_to=user.email, email=email, token=password_reset_token
#     )
#     return {"msg": "Password recovery email sent"}

# AIzaSyDFtEmPchLrmuNbtDnrXXIol0qlDFxrQ-E
# Client secret for ouath2!!!
# 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

@router.post("/reset-password/")
def reset_password(
    token: str = Body(...),
    new_password: str = Body(...),
    db: Session = Depends(get_db),
    user: schemas.User = Depends(get_current_user)
) -> Any:
    """
    Reset password
    """
    email = verify_password_reset_token(token)
    if not email:
        raise HTTPException(status_code=400, detail="Invalid token")
    user = crud.user.get_by_email(db, email=email)
    if not user:
        raise HTTPException(
            status_code=404,
            detail="The user with this username does not exist in the system.",
        )
    elif not crud.user.is_active(user):
        raise HTTPException(status_code=400, detail="Inactive user")
    hashed_password = get_password_hash(new_password)
    user.hashed_password = hashed_password
    db.add(user)
    db.commit()
    return {"msg": "Password updated successfully"}



@router.post("/google", response_model=schemas.Token)
def login_google_token(
    response: Response,  user_in: schemas.UserGoogle, db: Session = Depends(get_db),
) -> Any:
    """
    OAuth2 compatible token login, get an access token for future requests
    """
    security.check_google_token(user_in.id_token)
    user = crud.user.get_user_by_email(db, user_in.email)
    if not user:
        obj_in = schemas.UserGoogleCreate(user_in)
        user = crud.user.create_external(db, obj_in=obj_in)
    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    scopes = []
    if user.uses_otp:
        scopes.append('2fa')
    else:
        scopes = get_user_scopes(user)
    access_token = security.create_access_token(
        {"sub": str(user.id), "scopes": scopes}, expires_delta=access_token_expires)
    return {
        "access_token": access_token,
        "token_type": "bearer",
        "scopes": scopes,
    }