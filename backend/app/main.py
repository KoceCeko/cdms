from fastapi import FastAPI
from utils import get_settings
from fastapi.responses import HTMLResponse
from fastapi.middleware.cors import CORSMiddleware
from api.api import api_router

app = FastAPI(title='cdms')

origins = [
    "http://front.local.cdms.com",
    "https://front.local.cdms.com",
    "https://admin.local.cdms.com",
    "http://localhost",
    "http://localhost:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(api_router)


@app.get("/info")
async def info():
    return get_settings()


@app.get("/content")
async def root():
    content = """
        <div>Test 2</div>
        <body>
        <form action="/files/" enctype="multipart/form-data" method="post">
        <input name="files" type="file" multiple>
        <input type="submit">
        </form>
        <form action="/uploadfiles/" enctype="multipart/form-data" method="post">
        <input name="files" type="file" multiple>
        <input type="submit">
        </form>
        </body>
    """
    return HTMLResponse(content=content)