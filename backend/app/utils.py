import secrets
from functools import lru_cache
from pydantic import BaseSettings

class Settings(BaseSettings):
    password: str
    app_name: str
    username: str
    postgres_db: str
    postgres_server: str
    postgres_port: str
    postgres_database_url: str = "postgresql://postgres:postgres@postgres/cdms_db"
    SECRET_KEY: str = secrets.token_urlsafe(32)
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 24 * 8


    class Config:
        env_file = '../../cDMS/backend/app/.env'

        # f"postgresql://{username}:{password}@{postgres_server}:{postgres_port}/{postgres_db}"
        # self.postgres_database_url = f"postgresql://{self.username}:{self.password}@{self.postgres_server}:{self.postgres_port}/{self.postgres_db}"


settings = Settings()
@lru_cache()
def get_settings():
    return settings
