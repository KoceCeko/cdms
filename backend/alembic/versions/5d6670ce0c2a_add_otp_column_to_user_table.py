"""Add otp column to user table

Revision ID: 5d6670ce0c2a
Revises: 28e345998173
Create Date: 2022-03-14 19:18:18.158536

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5d6670ce0c2a'
down_revision = '28e345998173'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('users', sa.Column('otp_secret', sa.VARCHAR(32)))
    op.add_column('users', sa.Column('uses_otp', sa.Boolean))


def downgrade():
    op.drop_column('users', 'uses_otp')
    op.drop_column('users', 'otp_secret')
