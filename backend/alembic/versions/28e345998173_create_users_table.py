"""create users table

Revision ID: 28e345998173
Revises: 
Create Date: 2022-01-15 17:26:42.484670

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '28e345998173'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('users',
                    sa.Column('id', sa.Integer, primary_key=True),
                    sa.column('email', sa.String),
                    sa.Column('first_name', sa.String),
                    sa.Column('last_name', sa.String),
                    sa.Column('hashed_password', sa.VARCHAR(64)),
                    sa.Column('role', sa.Integer),
                    sa.Column('root_dir', sa.String),
                    sa.Column('is_active', sa.Boolean))


def downgrade():
    op.drop_table('users')
    pass

# /documents/ <--- root dir
# /documents/folder <--- root/folder